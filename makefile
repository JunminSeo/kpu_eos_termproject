CC = gcc --std=gnu99
TARGET_SERVER = server
TARGET_CLIENT = client
OBJS_SERVER = server.o credential.o epollcntl.o clicntl.o msg.o
OBJS_CLIENT = client.o epollcntl.o msg.o filecntl.o
SRCS_SERVER = $(OBJS_SERVER:.o=.c)
SRCS_CLIENT = $(OBJS_CLIENT:.o=.c)

all: $(TARGET_SERVER) $(TARGET_CLIENT)

$(TARGET_SERVER): $(OBJS_SERVER)
	$(CC) -o $@ $^ -lpthread

$(TARGET_CLIENT): $(OBJS_CLIENT)
	$(CC) -o $@ $^ -lpthread

clean:
	rm -f $(OBJS_SERVER) $(TARGET_SERVER)
	rm -f $(OBJS_CLIENT) $(TARGET_CLIENT)