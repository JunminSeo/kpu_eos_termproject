#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "clicntl.h"

char *str_cli_state[] = { "CLIENT_ERROR", "CLIENT_AUTH", "CLIENT_GRANT", "CLIENT_DENIED" };

/*
 * Public methods
 */
int init_client_pool(client_t *cl) {
	if (cl != NULL) {
		cl->idx = 0;
	} else {
		return -1;
	}
	
	return 0;
}

int add_client(client_t *cl, int sock_fd) {
	if (cl->idx < MAX_CLIENT) {
		cl->cli[cl->idx].state = CLIENT_AUTH;
		cl->cli[cl->idx].sock_fd = sock_fd;
		cl->idx++;
	} else {
		return -1;
	}
	
	return 0;
}

int del_client(client_t *cl, int sock_fd) {
	int idx_cli;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		perror("Failed query_client()");
		return -1;
	}
	
	memcpy((struct _client_info*)&(cl->cli[idx_cli]), (struct _client_info*)&(cl->cli[idx_cli + 1]), \
			 sizeof(*(cl->cli)) * (MAX_CLIENT - idx_cli - 1));
	cl->idx--;
    
	return 0;	
}

int set_client_username(client_t *cl, int sock_fd, const char *id, size_t len_id) {
	int idx_cli;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		perror("Failed query_client()");
		return -1;
	}
	
	strncpy(cl->cli[idx_cli].username, id, len_id);
	cl->cli[idx_cli].username[len_id] = '\0'; // Nul termination
	
	return 0;
}
int set_client_ipaddr(client_t *cl, int sock_fd, const char *ipstring, size_t len_ipstring) {
	int idx_cli;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		perror("Failed query_client()");
		return -1;
	}
	
	strncpy(cl->cli[idx_cli].ipaddr, ipstring, len_ipstring);
	cl->cli[idx_cli].ipaddr[len_ipstring] = '\0'; // Nul termination
	
	return 0;
}

int get_num_client(const client_t *cl) {
	return cl->idx;
}

int get_client_username(const client_t *cl, int sock_fd, char *str, size_t size_str) {
	int idx_cli;
	size_t len_username;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		return -1;
	}
	
	len_username = strlen(cl->cli[idx_cli].username);
	strncpy(str, cl->cli[idx_cli].username, len_username);
	
	str[len_username] = '\0'; // Nul termination
	
	return 0;
};

int get_client_ipaddr(const client_t *cl, int sock_fd, char *str, size_t size_str) {
	int idx_cli;
	size_t len_ipaddr;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		return -1;
	}
	
	len_ipaddr = strlen(cl->cli[idx_cli].ipaddr);
	strncpy(str, cl->cli[idx_cli].ipaddr, size_str);
	
	str[len_ipaddr] = '\0'; // Nul termination
	
	return 0;
}

int get_client_sockfd(const client_t *cl, int idx_cli) {
	return (idx_cli < cl->idx) ? (cl->cli[idx_cli].sock_fd) : -1;
}

cli_state_t get_client_state(const client_t *cl, int sock_fd) {
	int idx_cli;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		perror("Failed query_client()");
		return 0;
	}
	return cl->cli[idx_cli].state;
}

int set_client_state(client_t *cl, int sock_fd, cli_state_t state) {
	int idx_cli;
	
	idx_cli = query_client(cl, sock_fd);
	if (idx_cli == -1) {
		perror("Failed query_client()");
		return -1;
	}
	
	cl->cli[idx_cli].state = state;
	
	return 0;
}

int query_client_username(const client_t *cl, const char *username, size_t len_username) {
	size_t length;
	
	for (int i = 0; i < cl->idx; i++) {
		length = (strlen(cl->cli[i].username) < len_username) ? (len_username) : (strlen(cl->cli[i].username));
		
		if (strncmp(cl->cli[i].username, username, length) == 0) {
			return i;
		}
	}
	
	return -1;
}

/*
 * Private methods
 */
int query_client(const client_t *cl, int sock_fd) {
	for (int i = 0; i < cl->idx; i++) {
		if (cl->cli[i].sock_fd == sock_fd) {
			return i;
		}
	}
	
	return -1;
}
