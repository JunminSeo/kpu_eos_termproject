#include <stdio.h>
#include <stdlib.h>

#include <sys/epoll.h>
#include <fcntl.h>

/*
 * Public methods
 */
int init_epoll(int epoll_size) {
    // Create new epoll
    int epoll_fd = epoll_create(epoll_size);

    if (epoll_fd == -1) {
        perror("Failed epoll_create()");
        return -1;
    }
    
    printf("Create epoll is OK...\n");

    return epoll_fd;
}

int add_epoll_event(int epoll_fd, int client_fd) {
    struct epoll_event ev;

    ev.events = EPOLLIN;
    ev.data.fd = client_fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, client_fd, &ev) == -1) {
        perror("Failed epoll_ctl()");
        return -1;
    } else {
        printf("Add socket(#%d) to epoll event is OK...\n", client_fd);
    }

    return 0;
}

int set_socket_nonblock(int sock_fd) {
    int flags = fcntl(sock_fd, F_GETFL);
    flags |= O_NONBLOCK;

    if (fcntl(sock_fd, F_SETFL, flags) == -1) {
        perror("Failed fcntl()");
        return -1;
    } else {
        printf("Server fcntl() is OK... socket(#%d) is now nonblock\n", sock_fd);
    }

    return 0;
}