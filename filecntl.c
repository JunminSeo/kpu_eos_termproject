#include "filecntl.h"

#include <string.h>

#include <unistd.h>
#include <sys/stat.h>

#define MAX_PATH_SIZE 512

static const char *STORAGE_DIR = "./storage/";
static const char *DOWNLOAD_DIR = "./downloads/";

/*
 * Public methods
 */
void testPath_storage(void) {
	struct stat st;
	
	if (stat(STORAGE_DIR, &st) == -1) {
		mkdir(STORAGE_DIR, 0700);
	}
}

void testPath_download(void) {
	struct stat st;
	
	if (stat(DOWNLOAD_DIR, &st) == -1) {
		mkdir(DOWNLOAD_DIR, 0700);
	}
}

bool isFileExist_storage(const char *filename, size_t len_filename) {
	int len_path;
	char fullFilePath[MAX_PATH_SIZE] = { '\0' };
	
	len_path = getFullFilePath_storage(filename, len_filename, fullFilePath, sizeof(fullFilePath));
	if (len_path == -1) {
		return false;
	}
	
	if (access(fullFilePath, F_OK) == -1) {
		return false;
	}
	
	return true;
}

ssize_t getFullFilePath_storage(const char *filename, size_t len_filename, char *fullFilePath, size_t size_fullFilePath) {
	ssize_t len_path;
	
	len_path = getFullFilePath(STORAGE_DIR, filename, len_filename, fullFilePath, size_fullFilePath);
	if ( len_path == -1) {
		return -1;
	}
	
	return len_path;
}

ssize_t getFullFilePath_download(const char *filename, size_t len_filename, char *fullFilePath, size_t size_fullFilePath) {
	ssize_t len_path;
	
	len_path = getFullFilePath(DOWNLOAD_DIR, filename, len_filename, fullFilePath, size_fullFilePath);
	if ( len_path == -1) {
		return -1;
	}
	
	return len_path;
}

/*
 * Private methods
 */
ssize_t getFullFilePath(const char *path, const char *filename, size_t len_filename, char *fullFilePath, size_t size_fullFilePath) {
	ssize_t len_path;
	
	len_path = strlen(path) + len_filename;
	if (len_path > size_fullFilePath) {
		return -1;
	}
	
	strncat(fullFilePath, path, strlen(path));
	strncat(fullFilePath, filename, len_filename);
	fullFilePath[len_path] = '\0';
	
	return len_path;
}