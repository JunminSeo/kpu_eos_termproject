#pragma once

#include <stdbool.h>

#include <sys/types.h>

enum _msg_type {
	MSG_CHAT = 0, MSG_CRED, MSG_FILE, MSG_CLOSE
};

enum _msg_type_query {
	MSG_QUERY_USER = 0
};

enum _msg_type_file {
	MSG_FILE_LS_REQ = 0, MSG_FILE_LS_START, MSG_FILE_LS_BODY, MSG_FILE_LS_END,
	MSG_FILE_DN_REQ, MSG_FILE_DN_ACK, MSG_FILE_DN_NACK
};

extern const char *msg_cmd[];

extern ssize_t sendMsg(int, const char*, size_t);
extern ssize_t sendCred(int, const char*, size_t, const char*, size_t);
extern ssize_t sendBye(int);
extern ssize_t sendQueryUser(int, const char*, size_t);
extern ssize_t sendFileLsReq(int, const char*, size_t);
extern int sendFileLs(int, const char*, size_t);

extern ssize_t sendFileDnReq(int, const char*, size_t, const char*, size_t);
extern ssize_t sendFileDnAck(int, const char*, size_t, const char*, size_t, const char*, size_t, const char*, size_t);
extern ssize_t sendFileDnNack(int, const char*, size_t);

extern bool iscmd(const char*, size_t);

extern ssize_t sendFileLs_start(int, const char*, size_t);
extern ssize_t sendFileLs_body(int, const char*, size_t, const char*, size_t);
extern ssize_t sendFileLs_end(int, const char*, size_t);