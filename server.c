#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h>

#include "credential.h"
#include "epollcntl.h"
#include "clicntl.h"
#include "msg.h"

#define arrlengthof(x) (sizeof(x) / sizeof((x)[0]))

/*
 * 전역 상수
 */
#define BUFF_SIZE 512

static const int EPOLL_EVENT_SIZE = 15;
static const unsigned short SERV_PORT = 4210;
static const int BACKLOG = 10;

static const char *INIT_MSG = "==============================\n"
							  "Hello P2P File Sharing Server...\n"
							  "Please, LOG-IN!\n"
					  		  "==============================\n";

/*
 * 전역 변수
 */
// Client pool
client_t Client_info;
pthread_mutex_t Mutex_lock;

/*
 * 함수 선언
 */
int init_server(unsigned short, int);

int client_msg_process(int, client_t*, const char*, size_t);

// MISC
credential_t parseCred(const char*, size_t);
void broadcast_client_msg(int, client_t*, const char*, size_t);
int getCFTime(char*, size_t);
 
/*
 * Signal handler
 */
/*void sig_handler(int signo) {
    int status;
    pid_t pid;

    switch (signo) {
        /*
        case SIGINT:
            printf("SIGINT!\n");
            RUNNING = false;
            break;
        *
        case SIGCHLD:
            while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
                printf("Exit child %d\n", pid);
            }
            break;
        default:
            break;
    }
}*/

/*
 * Thread
 */
void *client_thread(void *data) {
	int client_sockfd = *((int*)data);
	
	ssize_t rcv_size;
	char buf[BUFF_SIZE];
	
	do {
		rcv_size = recv(client_sockfd, buf, sizeof(buf) - 1, 0);
		if (rcv_size == -1) {
			fprintf(stderr, "%d Failed recv()\n", __LINE__);
			
			goto fail;
			//close(client_sockfd); // epoll event also deleted automatically
		} else if (rcv_size == 0) {
			printf("Gentle close\n");
			
			goto success;
		} else {
			buf[rcv_size] = '\0'; // Nul termination
			printf("received: %zd %s\n", rcv_size, buf);
		}
		
		if (client_msg_process(client_sockfd, &Client_info, buf, strlen(buf)) == -1) {
			fprintf(stderr, "%d Failed client_msg_process()\n", __LINE__);
			
			goto fail;
		}
	} while(rcv_size);

success:
	printf("Thread exit success\n");
fail:
	printf("Thread exit fail\n");
}

/*
 * MAIN
 */
int main(void) {
	// Epoll
	int epoll_fd;
	struct epoll_event *events;
	
    // Socket
    int server_sockfd;
	int client_sockfd;
	
	// Client Socket info
	struct sockaddr_in addr;
	int addr_len = sizeof(struct sockaddr_in);
	
	// Buffer
	char ipaddr[INET_ADDRSTRLEN];
	char buf[BUFF_SIZE];
	char username[BUFF_SIZE];
	
	// Thread
	pthread_t p_thread;
	
    /* Assign signal handler */
    //signal(SIGINT,(void*)sig_handler);
    //signal(SIGCHLD, (void*)sig_handler);

    /* Init server */
	server_sockfd = init_server(SERV_PORT, BACKLOG);
    if (server_sockfd == -1) {
		fprintf(stderr, "%d Failed to start server\n", __LINE__);

        goto fail;
    } else {
        printf("Server started!\n");
    }
	
	// Init client pool
	if (init_client_pool(&Client_info) == -1) {
		fprintf(stderr, "%d Failed init_client_pool()\n", __LINE__);
		
		goto fail;
	}
	
	// Init Mutex
	if (pthread_mutex_init(&Mutex_lock, NULL) == -1) {
		fprintf(stderr, "%d Failed init_mutex_init()\n", __LINE__);
		
		goto fail;
	}

	/* Server process */
	while (true) {
		client_sockfd = accept(server_sockfd, (struct sockaddr*)&addr, &addr_len);
		if (client_sockfd == -1) {
			fprintf(stderr, "%d Failed accept()\n", __LINE__);
			
			goto fail;
		} else {
			// Get client ip string
			inet_ntop(AF_INET, &addr.sin_addr.s_addr, buf, sizeof(buf));
			strncpy(ipaddr, buf, INET_ADDRSTRLEN);
			
			// Get formatted time string
			getCFTime(buf, BUFF_SIZE);
			printf("Connection requested from: %s %s\n", ipaddr, buf);
		}

		// Send welcome message to client
		send(client_sockfd, INIT_MSG, strlen(INIT_MSG) + 1, 0);

		// Add client info
		pthread_mutex_lock(&Mutex_lock);
		if (add_client(&Client_info, client_sockfd) == -1) {
			fprintf(stderr, "%d Failed add_client()\n", __LINE__);
			
			pthread_mutex_unlock(&Mutex_lock);
			close(client_sockfd);
			
			goto fail;
		} else {
			pthread_mutex_unlock(&Mutex_lock);
			
			if (get_client_username(&Client_info, client_sockfd, username, sizeof(username)) == -1) {
				fprintf(stderr, "%d Failed get_client_username()\n", __LINE__);
				
				goto fail;
			} else {
				printf("Register client : %s\n", username);
			}
		}

		// Add client ip address
		pthread_mutex_lock(&Mutex_lock);
		if (set_client_ipaddr(&Client_info, client_sockfd, ipaddr, strlen(ipaddr)) == -1) {
			fprintf(stderr, "%d Failed set_client_ipaddr()\n", __LINE__);
			
			pthread_mutex_unlock(&Mutex_lock);
			close(client_sockfd);
			
			goto fail;
		} else {
			pthread_mutex_unlock(&Mutex_lock);
			
			printf("Set client ip address\n");
		}
		
		/* FOR MULTI THREAD */
		// Create new thread
		printf("Create new thread\n");
		pthread_create(&p_thread, NULL, client_thread, (void*)&client_sockfd);
		pthread_detach(p_thread);
	}
   
success:
    return 0;
	
fail:
	return -1;
}

int init_server(unsigned short port, int backlog) {
	// Listen on sock_fd
	int sock_fd;

	// My address information
	struct sockaddr_in my_addr;
	
	// Create new socket
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_fd == -1) {
		fprintf(stderr, "%d Failed socket()\n", __LINE__);
		
		goto fail;
	} else {
		printf("Server socket() sock_fd is OK...\n");
	}

	// Host byte order
	my_addr.sin_family = AF_INET;

	// Short, network byte order
	my_addr.sin_port = htons(port);
	my_addr.sin_addr.s_addr = INADDR_ANY;

	// Zero the rest of the struct
	memset(&(my_addr.sin_zero), 0, sizeof(my_addr.sin_zero));

	// Bind
	if (bind(sock_fd, (struct sockaddr*)&my_addr, sizeof(struct sockaddr)) == -1) {
		fprintf(stderr, "%d Failed bind()\n", __LINE__);
		
		close(sock_fd);
		
		goto fail;
	} else {
		printf("Server bind() is OK...\n");
	}

	// Listen
	if (listen(sock_fd, backlog) == -1) {
		fprintf(stderr, "%d Failed listen()\n", __LINE__);
		
		close(sock_fd);
		
		goto fail;
	} else {
		printf("Server listen() is OK...\n");
	}

success:
	return sock_fd;
	
fail:
	return -1;
}

int client_msg_process(int client_sockfd, client_t *client_info, const char *msg, size_t len_msg) {
	// Buffer
	ssize_t rcv_size;
	char buf[BUFF_SIZE];
	char username[BUFF_SIZE];
	
	// Credential
	credential_t remote_cred;
	
	/* Client event */
	switch (get_client_state(client_info, client_sockfd)) {
	default:
	case CLIENT_AUTH:
		if (msg[0] == MSG_CRED + '0') {
			printf("This message is MSG_CRED\n");
			
			// Parse credential info
			remote_cred = parseCred(msg + 1, strlen(msg + 1));
			
			/* Auth */
			// Access grant
			if (auth(remote_cred.username, strlen(remote_cred.username), \
			 remote_cred.password, strlen(remote_cred.password))) {
				getCFTime(buf, BUFF_SIZE);
				printf("Access granted %s %s\n", remote_cred.username, buf);
				snprintf(buf, BUFF_SIZE, "Welcome, %s", remote_cred.username);
				sendMsg(client_sockfd, buf, strlen(buf));
				
				pthread_mutex_lock(&Mutex_lock);
				if (set_client_username(client_info, client_sockfd, remote_cred.username, strlen(remote_cred.username)) == -1) {
					fprintf(stderr, "%d Failed set_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					goto fail;
				}
				pthread_mutex_unlock(&Mutex_lock);
				
				pthread_mutex_lock(&Mutex_lock);
				if (set_client_state(client_info, client_sockfd, CLIENT_GRANT) == -1) {
					fprintf(stderr, "%d Failed set_client_state()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					goto fail;
				} else {
					pthread_mutex_unlock(&Mutex_lock);
					printf("%s state is now %s\n", remote_cred.username, str_cli_state[get_client_state(client_info, client_sockfd)]);
				}
			} else {
				getCFTime(buf, BUFF_SIZE);
				printf("Access denied %s %s\n", remote_cred.username, buf);
				snprintf(buf, BUFF_SIZE, "Access denied!");
				sendMsg(client_sockfd, buf, strlen(buf));
				
				pthread_mutex_lock(&Mutex_lock);
				if (set_client_state(client_info, client_sockfd, CLIENT_DENIED) == -1) {
					fprintf(stderr, "%d Failed set_client_state()\n", __LINE__);
				}
				pthread_mutex_unlock(&Mutex_lock);
				
				goto fail;
			}
		} else {
			// Failed
			printf("This message is not MSG_CRED\n");
			
			goto fail;
		}
		break;
	case CLIENT_GRANT:
		pthread_mutex_lock(&Mutex_lock);
		if (get_client_username(client_info, client_sockfd, username, sizeof(username)) == -1) {
			fprintf(stderr, "%d Failed get_client_username()\n", __LINE__);
			
			pthread_mutex_unlock(&Mutex_lock);
			
			goto fail;
		}
		pthread_mutex_unlock(&Mutex_lock);
		
		// MSG_CHAT
		if (msg[0] == MSG_CHAT + '0') {
			snprintf(buf, BUFF_SIZE, "%s: %s", username, msg + 1);
			printf("%s\n", buf);
			
			// Broadcast to other clients
			pthread_mutex_lock(&Mutex_lock);
			broadcast_client_msg(client_sockfd, client_info, buf, strlen(buf));
			pthread_mutex_unlock(&Mutex_lock);
		// MSG_FILE
		} else if (msg[0] == MSG_FILE + '0') {
			char *pbuf;

			strncpy(buf, msg, len_msg);
			buf[len_msg] = '\0';
			
			pbuf = strtok(buf, " ");
			
			// MSG_FILE_LS_REQ
			if (msg[1] == MSG_FILE_LS_REQ + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s", username_receiver);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					pthread_mutex_unlock(&Mutex_lock);
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileLsReq(sock_fd, username, strlen(username)) == -1) {
							fprintf(stderr, "%d Failed sendFileLsReq()\n", __LINE__);
							
							goto fail;
						}
					}
				}
			// MSG_FILE_LS_START
			} else if (msg[1] == MSG_FILE_LS_START + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s", username_receiver);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileLs_start(sock_fd, username, strlen(username)) == -1) {
							fprintf(stderr, "%d Failed sendFileLs_start()\n", __LINE__);
						
							goto fail;
						}
					}
				}
			// MSG_FILE_LS_BODY
			} else if (msg[1] == MSG_FILE_LS_BODY + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				char filename[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pbuf = strtok(NULL, " ");
				snprintf(filename, BUFF_SIZE, "%s", pbuf);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);

					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s", pbuf + 2);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileLs_body(sock_fd, username, strlen(username), filename, strlen(filename)) == -1) {
							fprintf(stderr, "%d Failed sendFileLs_body()\n", __LINE__);
							
							goto fail;
						}
					}
				}
			// MSG_FILE_LS_END
			} else if (msg[1] == MSG_FILE_LS_END + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s", username_receiver);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileLs_end(sock_fd, username, strlen(username)) == -1) {
							fprintf(stderr, "%d Failed sendFileLs_end()\n", __LINE__);
							
							goto fail;
						}
					}
				}
			// MSG_FILE_DN_REQ
			} else if (msg[1] == MSG_FILE_DN_REQ + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				char filename[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pbuf = strtok(NULL, " ");
				snprintf(filename, BUFF_SIZE, "%s", pbuf);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s\n", username_receiver);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileDnReq(sock_fd, username, strlen(username), filename, strlen(filename)) == -1) {
							fprintf(stderr, "%d Failed sendFileDnReq()\n", __LINE__);
							
							goto fail;
						}
					}
				}
			// MSG_FILE_DN_ACK
			} else if (msg[1] == MSG_FILE_DN_ACK + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				char filename[BUFF_SIZE];
				char ipaddr[BUFF_SIZE];
				char port[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pbuf = strtok(NULL, " ");
				snprintf(filename, BUFF_SIZE, "%s", pbuf);
				
				pbuf = strtok(NULL, " ");
				//snprintf(ipaddr, BUFF_SIZE, "%s", pbuf);
				
				pthread_mutex_lock(&Mutex_lock);
				get_client_ipaddr(client_info, client_sockfd, ipaddr, BUFF_SIZE);
				pthread_mutex_unlock(&Mutex_lock);
				
				pbuf = strtok(NULL, " ");
				snprintf(port, BUFF_SIZE, "%s", pbuf);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s\n", username_receiver);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileDnAck(sock_fd, username, strlen(username), filename, strlen(filename),\
							 ipaddr, strlen(ipaddr), port, strlen(port)) == -1) {
							fprintf(stderr, "%d Failed sendFileDnAck()\n", __LINE__);
							
							goto fail;
						} else {
							printf("Send FileDnAck %s %s %s\n", username_receiver, username, filename);
						}
					}
				}
			// MSG_FILE_DN_NACK
			} else if (msg[1] == MSG_FILE_DN_NACK + '0') {
				// sock_fd, idx_cli - receiver
				// client_sockfd username[] - sender
				int sock_fd;
				int idx_cli;
				char username_receiver[BUFF_SIZE];
				
				snprintf(username_receiver, BUFF_SIZE, "%s", pbuf + 2);
				
				pthread_mutex_lock(&Mutex_lock);
				idx_cli = query_client_username(client_info, username_receiver, strlen(username_receiver));
				if (idx_cli == -1) {
					fprintf(stderr, "%d Failed query_client_username()\n", __LINE__);
					
					pthread_mutex_unlock(&Mutex_lock);
					
					snprintf(buf, BUFF_SIZE, "No such user %s\n", username_receiver);
					sendMsg(client_sockfd, buf, strlen(buf));
				} else {
					get_client_username(client_info, client_sockfd, username, sizeof(username));
					
					sock_fd = get_client_sockfd(client_info, idx_cli);
					if (sock_fd == -1) {
						fprintf(stderr, "%d Failed get_client_sockfd()\n", __LINE__);
						
						pthread_mutex_unlock(&Mutex_lock);
						
						goto fail;
					} else {
						pthread_mutex_unlock(&Mutex_lock);
						
						if (sendFileDnNack(sock_fd, username, strlen(username)) == -1) {
							fprintf(stderr, "%d Failed sendFileDnNack()\n", __LINE__);
							
							goto fail;
						} else {
							printf("Send FileDnNack %s %s\n", username_receiver, username);
						}
					}
				}
			// Unknown file command
			} else {
				printf("Unknown file command\n");
			}
		// MSG_CLOSE
		} else if (msg[0] == MSG_CLOSE + '0') {
			snprintf(buf, BUFF_SIZE, "%s has left", username);
			
			// Broadcast to other clients
			pthread_mutex_lock(&Mutex_lock);
			broadcast_client_msg(client_sockfd, client_info, buf, strlen(buf));
			pthread_mutex_unlock(&Mutex_lock);
			
			// Close client
			pthread_mutex_lock(&Mutex_lock);
			del_client(client_info, client_sockfd);
			pthread_mutex_unlock(&Mutex_lock);
			close(client_sockfd);
		} else {
			printf("Unknown command\n");
		}
		break;
	case CLIENT_DENIED:
		goto fail;
		
		break;
	}

success:
	return 0;
	
fail:
	return -1;
}

credential_t parseCred(const char *msg, size_t len_msg) {
	credential_t remote_cred;
	char buf[BUFF_SIZE];
	char *pbuffer;
	
	if (len_msg > BUFF_SIZE) {
		printf("parseCred too long!\n");
		
		goto toolong;
	} else {
		strncpy(buf, msg, len_msg);
		buf[len_msg] = '\0';
	}
	
	pbuffer = strtok(buf, " ");
    strncpy(remote_cred.username, pbuffer, strlen(pbuffer));
    remote_cred.username[strlen(pbuffer)] = '\0';
    pbuffer = strtok(NULL, " ");
    strncpy(remote_cred.password, pbuffer, strlen(pbuffer));
    remote_cred.password[strlen(pbuffer)] = '\0';
	
toolong:
success:
	return remote_cred;
}

void broadcast_client_msg(int client_sockfd, client_t *client_info, const char *msg, size_t len_msg) {
	for (int j = 0; j < get_num_client(client_info); j++) {
		if (client_sockfd != client_info->cli[j].sock_fd) {
			if (sendMsg(client_info->cli[j].sock_fd, msg, len_msg) == -1) {
				fprintf(stderr, "%d Failed send()\n", __LINE__);
				
				// Remove unavailable socket
				close(client_info->cli[j].sock_fd);
				del_client(client_info, client_info->cli[j].sock_fd);
			} else {
				printf("Send: %s\n", msg);
			}
		}
	}
}

int getCFTime(char *buff, size_t buff_size) {
    time_t tm_time;
    struct tm *st_time;
    
    time(&tm_time);
    st_time = localtime(&tm_time);

    strftime(buff, buff_size, "%Y.%m.%d - %p %l:%M:%S", st_time);
	
	return 0;
}