#pragma once

#define MAX_CLIENT 5

typedef enum _cli_state_t {
	CLIENT_ERROR = 0, CLIENT_AUTH, CLIENT_GRANT, CLIENT_DENIED
} cli_state_t;

struct _client_info {
	cli_state_t state;
	int sock_fd;
	char username[255];
	char ipaddr[255];
} _client_info;

typedef struct _client_t {
	int idx;
	struct _client_info cli[MAX_CLIENT];
} client_t;

extern char *str_cli_state[];

extern int init_client_pool(client_t*);
extern int add_client(client_t*, int);
extern int del_client(client_t*, int);	// Not implemented yet
extern int set_client_username(client_t*, int, const char*, size_t);
extern int set_client_ipaddr(client_t*, int, const char*, size_t);
extern int set_client_state(client_t*, int, cli_state_t);
extern int get_client_username(const client_t*, int, char*, size_t);
extern int get_client_ipaddr(const client_t*, int, char*, size_t);
extern int get_client_sockfd(const client_t*, int);
extern int get_num_client(const client_t*);
extern int query_client_username(const client_t*, const char*, size_t);
extern cli_state_t get_client_state(const client_t*, int);

static int query_client(const client_t*, int);
