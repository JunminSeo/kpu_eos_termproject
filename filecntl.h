#pragma once

#include <stdbool.h>

#include <sys/types.h>

extern void testPath_storage(void);
extern void testPath_download(void);
extern bool isFileExist_storage(const char*, size_t);
extern ssize_t getFullFilePath_storage(const char*, size_t, char*, size_t);
extern ssize_t getFullFilePath_download(const char*, size_t, char*, size_t);

static ssize_t getFullFilePath(const char*, const char*, size_t, char*, size_t);