#pragma once

extern int init_epoll(int);
extern int add_epoll_event(int, int);
extern int set_socket_nonblock(int);