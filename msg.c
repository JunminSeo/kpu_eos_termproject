#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <dirent.h>
#include <sys/socket.h>

#include "msg.h"

#define MAX_MSG_SIZE 512

const char *msg_cmd[] = {"chat", "signin", "file", "bye"};
const char *STORAGE_DIR = "./storage/";
/*
 * Public methods
 */
ssize_t sendMsg(int sock_fd, const char *msg, size_t len_msg) {
	// 1: MSG_TYPE 2: NUL
	ssize_t size_str = len_msg + 1;
	int offset = 1;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_CHAT + '0';
	
	strncpy(str + offset, msg, len_msg);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
    return size_str;
	
fail:
toolong:
	return -1;
}

ssize_t sendCred(int sock_fd, const char *id, size_t len_id, const char *pw, size_t len_pw) {
	// 1: MSG_TYPE 2: Space 3: NUL
	ssize_t size_str = len_id + len_pw + 2;
	int offset = 1;
	char str[MAX_MSG_SIZE];

	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_CRED + '0';
	
	strncpy(str + offset, id, len_id);
	
	offset += len_id;
	str[offset] = ' ';
    
	offset += 1;
	strncpy(str + offset, pw, len_pw);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}


success:	
    return size_str;

fail:
toolong:
	return -1;
}

ssize_t sendBye(int sock_fd) {
	// 1: MSG_TYPE 2: NUL
	ssize_t size_str = 1;
    const char str[] = { MSG_CLOSE + '0', '\0'};
    if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
        perror("Failed send()");
        ssize_t size_str = -1;
    }

    return size_str;
}

/*
ssize_t sendQueryUser(int sock_fd, const char *username, size_t len_username) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: NUL
	ssize_t size_str = len_username + 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_QUERY + '0';
	str[1] = MSG_QUERY_USER + '0';
	
	strncpy(str + 2, username, len_username);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
	return size_str;

fail:
toolong:
	return -1;
}
*/

ssize_t sendFileLsReq(int sock_fd, const char *username, size_t len_username) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: NUL
	ssize_t size_str = len_username + 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_LS_REQ + '0';
	
	strncpy(str + 2, username, len_username);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}

success:
	return size_str;

fail:
toolong:
	return -1;
}

int sendFileLs(int sock_fd, const char *username, size_t len_username) {
	DIR *d;
	struct dirent *dir;
	
	d = opendir(STORAGE_DIR);
	
	if (d != NULL) {
		if (sendFileLs_start(sock_fd, username, len_username) == -1) {
			goto fail;
		}
		
		while((dir = readdir(d)) != NULL) {
			if (dir->d_type == DT_REG) {
				if (sendFileLs_body(sock_fd, username, len_username, dir->d_name, strlen(dir->d_name)) == -1) {
					goto fail;
				}
			}
			
			usleep(50000);
		}
		
		closedir(d);
		
		if (sendFileLs_end(sock_fd, username, len_username) == -1) {
			goto fail;
		}
	} else {
		goto fail;
	}
	
success:
	return 0;
	
fail:
	return -1;
}

ssize_t sendFileLs_start(int sock_fd, const char *username, size_t len_username) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: NUL
	ssize_t size_str = len_username + 2;
	int offset = 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_LS_START + '0';
	
	strncpy(str + offset, username, len_username);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
	return size_str;

fail:
toolong:
	return -1;
}

ssize_t sendFileLs_body(int sock_fd, const char *username, size_t len_username, const char *filename, size_t len_filename) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: Space 4: NUL
	ssize_t size_str = len_username + len_filename + 3;
	int offset = 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_LS_BODY + '0';
	
	strncpy(str + offset, username, len_username);
	
	offset += len_username;
	str[offset] = ' ';
	offset++;
	
	strncpy(str + offset, filename, len_filename);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
	return size_str;
	
fail:
toolong:
	return -1;
}

ssize_t sendFileLs_end(int sock_fd, const char *username, size_t len_username) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: NUL
	ssize_t size_str = len_username + 2;
	int offset = 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_LS_END + '0';
	
	strncpy(str + offset, username, len_username);
	str[size_str] = '\0';
	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
	return size_str;

fail:
toolong:
	return -1;
}

ssize_t sendFileDnReq(int sock_fd, const char *username, size_t len_username, const char *filename, size_t len_filename) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: SPACE 4: NUL
	ssize_t size_str = len_username + len_filename + 3;
	int offset = 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_DN_REQ + '0';
	
	strncpy(str + offset, username, len_username);
	offset += len_username;
	
	str[offset] = ' ';
	offset++;
	
	strncpy(str + offset, filename, len_filename);
	
	str[size_str] = '\0';
	
	printf("sendFileDnReq: %zd %s\n", size_str, str);
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
	return size_str;
	
fail:
toolong:
	return -1;
}

ssize_t sendFileDnAck(int sock_fd, const char *username, size_t len_username, const char *filename, size_t len_filename, const char *ipaddr, size_t len_ipaddr, const char *port, size_t len_port) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: SPACE 4: SPACE 5: SPACE 6: NUL
	ssize_t size_str = len_username + len_filename + len_ipaddr + len_port + 5;
	int offset = 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}

	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_DN_ACK + '0';
	
	strncpy(str + offset, username, len_username);
	offset += len_username;
	
	str[offset] = ' ';
	offset++;
	
	strncpy(str + offset, filename, len_filename);
	offset += len_filename;
	
	str[offset] = ' ';
	offset++;
	
	strncpy(str + offset, ipaddr, len_ipaddr);
	offset += len_ipaddr;
	
	str[offset] = ' ';
	offset++;
	
	strncpy(str + offset, port, len_port);
	str[size_str] = '\0';
	
	printf("%zd %s\n", size_str, str);
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}

success:
	return size_str;
	
fail:
toolong:
	return -1;	
}

ssize_t sendFileDnNack(int sock_fd, const char *username, size_t len_username) {
	// 1: MSG_TYPE 2: MSG_TYPE_SUB 3: NUL
	ssize_t size_str = len_username + 2;
	int offset = 2;
	char str[MAX_MSG_SIZE];
	
	if (size_str > MAX_MSG_SIZE) {
		goto toolong;
	}
	
	str[0] = MSG_FILE + '0';
	str[1] = MSG_FILE_DN_NACK + '0';
	
	strncpy(str + offset, username, len_username);
	str[size_str] = '\0';

	
	if (send(sock_fd, str, strlen(str) + 1, 0) == -1) {
		goto fail;
	}
	
success:
	return size_str;
	
fail:
toolong:
	return -1;
}

bool iscmd(const char *msg, size_t len_msg) {
    return (msg[0] == '/') ? true : false;
}