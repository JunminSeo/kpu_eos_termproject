#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "credential.h"

#define arrlengthof(x) (sizeof(x) / sizeof((x)[0]))

static const int BUFF_SIZE = 512;

/*
 * Public methods
 */
bool auth(const char *id, size_t len_id, const char *pw, size_t len_pw) {
	bool access_grant;
	
	FILE *fp;
	
	credential_t *local_cred;
	
	fp = fopen("passwd", "r");
    local_cred = getUserInfo(fp, id, len_id);
    fclose(fp);

	if (local_cred != NULL) {
		access_grant = \
		 (strncmp(pw, local_cred->password, len_pw) == 0) ? true : false;
		free(local_cred);
	} else {
		access_grant = false;
	}
	
	return access_grant;
}

/*
 * Private methods
 */
credential_t* getUserInfo(FILE *fp, const char *username, size_t size) {
	char temp[BUFF_SIZE];
	char *pStr;
    size_t loc_return;

	credential_t *result = (credential_t*)malloc(sizeof(credential_t));

	while(fgets(temp, arrlengthof(temp), fp)) {
		// Parse username
		pStr = strtok(temp, ":");
		if (pStr != NULL) {
			strncpy(result->username, pStr, arrlengthof(result->username) - 1);
		} else {
			return NULL;
		}

		// Parse password
		pStr = strtok(NULL, ":");
		if (pStr != NULL) {
			strncpy(result->password, pStr, strlen(pStr));
            result->password[strlen(pStr) - 1] = '\0';
		} else {
			return NULL;
		}
		
		if (strncmp(result->username, username, arrlengthof(result->username)) == 0) {
			return result;
		}
	}

	return NULL;
}
