#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <unistd.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include "credential.h"
#include "epollcntl.h"
#include "msg.h"
#include "filecntl.h"

#define arrlengthof(x) (sizeof(x) / sizeof(*x))

/*
 * 전역 상수
 */
#define BUFF_SIZE 512
static const int EPOLL_EVENT_SIZE = 3;

static const char *SERV_IP = "220.149.128.102";
static const int SERV_PORT = 4210;
static const int P2P_PORT = 4211;
static const int BACKLOG = 5;

/*
 * 쓰래드 파라미터
 */
struct tparam {
	int sock_fd;
	char filename[BUFF_SIZE];
};

/*
 * 쓰래드
 */
void *p2p_thread_sender(void *data) {
	FILE *fp = NULL;
	size_t size_file;
	char file_buf[BUFF_SIZE];
		
	ssize_t rcv_size;
	ssize_t len_filepath;
	char filename[BUFF_SIZE];
	char fullFilePath[BUFF_SIZE] = { '\0' };
	
	// Socket
	int server_sockfd = *((int*)data);
	int client_sockfd;
	
	// Connection info
	struct sockaddr_in addr;
	int addr_len = sizeof(struct sockaddr_in);
	
	client_sockfd = accept(server_sockfd, (struct sockaddr*)&addr, &addr_len);
	if (client_sockfd == -1) {
		perror("Failed accept()");
		goto exit;
	}
	
	rcv_size = recv(client_sockfd, filename, sizeof(filename), 0);
	if (rcv_size == -1) {
		perror("Failed recv()");
		goto exit;
	} else if (rcv_size == 0) {
		printf("Gentle close\n");
		goto exit;
	} else {
		filename[rcv_size] = '\0';
	}
	
	len_filepath = getFullFilePath_storage(filename, strlen(filename), fullFilePath, sizeof(fullFilePath));
	if (len_filepath == -1) {
		perror("Failed getFullFilePath()");
		goto exit;
	}
	
	fp = fopen(fullFilePath, "r");
	if (fp == NULL) {
		perror("Failed fopen()");
		goto exit;
	}
	
	do {
		size_file = fread(file_buf, sizeof(char), sizeof(file_buf), fp);
		
		if (send(client_sockfd, file_buf, size_file, 0) == -1) {
			perror("Failed send()");
			break;
		}
		
		// Clear buffer
		memset(file_buf, '\0', sizeof(file_buf));
	} while (size_file);
	
exit:
	if (fp != NULL) {
		fclose(fp);
	}
	close(client_sockfd);
	
	printf("Thread exit\n");
}

void *p2p_thread_receiver(void *data) {
	struct tparam *param = (struct tparam*)data;
	
	FILE *fp;
	ssize_t rcv_size;
	ssize_t path_size;
	char fullFilePath[BUFF_SIZE] = { '\0' };
	char file_buf[BUFF_SIZE];
	
	testPath_download();
	
	path_size = getFullFilePath_download(param->filename, strlen(param->filename), fullFilePath, sizeof(fullFilePath));
	if (path_size == -1) {
		perror("Failed getFullFilePath_download()");
		goto exit;
	}
	
	fp = fopen(fullFilePath, "w");
	if (fp == NULL) {
		perror("Failed fopen()");
		goto exit;
	}
	
	if (send(param->sock_fd, param->filename, strlen(param->filename) + 1, 0) == -1) {
		perror("Failed send()");
		goto exit;
	}
	
	do {
		rcv_size = recv(param->sock_fd, file_buf, sizeof(file_buf), 0);
		if (rcv_size == -1) {
			perror("Failed recv()");
			goto exit;
		} else if (rcv_size == 0) {
			printf("File transfer is complete\n");
		}
		
		// Write to file
		fwrite(file_buf, sizeof(char), rcv_size, fp);
		
		// Clear buffer
		memset(file_buf, '\0', sizeof(file_buf));
	} while (rcv_size);
	
exit:
	if (fp != NULL) {
		fclose(fp);
	}
	close(param->sock_fd);
	
	printf("Thread exit\n");
}

/*
 * 함수 선언
 */
int connect_server(const char*, unsigned short);
int init_server(unsigned short, int);

/*
 * MAIN
 */
int main(void) {
	// Epoll
	int epoll_fd;
	struct epoll_event *events;
	
	// Socket
    int sock_fd;
	
	// Buffer
	ssize_t rcv_size;
	char buf[BUFF_SIZE];
	char msg[BUFF_SIZE];
	
	// Credential
    char id[BUFF_SIZE];
    char pw[BUFF_SIZE];

	/* Init epoll */
	epoll_fd = epoll_create(EPOLL_EVENT_SIZE);
	events = malloc(sizeof(struct epoll_event) * EPOLL_EVENT_SIZE);
	
	/* Connect to server */
	sock_fd = connect_server(SERV_IP, SERV_PORT);
	if (sock_fd == -1) {
		perror("Failed to connect server");
		exit(EXIT_FAILURE);
	}
	
	// Receive INIT_MSG
	rcv_size = recv(sock_fd, buf, sizeof(buf), 0);
	if (rcv_size == -1) {
		perror("Failed recv()");
		exit(EXIT_FAILURE);
	} else {
		printf("%s\n", buf);
	}

	/* Send credential */
    printf("ID: ");
    scanf("%s", id);
	
    printf("PW: ");
    scanf("%s", pw);
    //pw = getpass("PW: ");
    
	sendCred(sock_fd, id, strlen(id), pw, strlen(pw));
	
	/* Add epoll event */
	// Add stdin to epoll event
	add_epoll_event(epoll_fd, STDIN_FILENO);
    
	// Add socket to epoll event
    set_socket_nonblock(sock_fd);
    add_epoll_event(epoll_fd, sock_fd);
	
	while (true) {
		// Epoll wait
		int event_count = epoll_wait(epoll_fd, events, EPOLL_EVENT_SIZE, -1);
		if (event_count == -1) {
			perror("Failed epoll_wait()");
			exit(EXIT_FAILURE);
		}
		
		/* Epoll event */
		for (int i = 0; i < event_count; i++) {
			/* Stdin event */
			if (events[i].data.fd == STDIN_FILENO) {
				rcv_size = read(STDIN_FILENO, buf, sizeof(buf));
				if (rcv_size == -1) {
					perror("Failed read() from stdin");
					exit(EXIT_FAILURE);
				} else {
					buf[rcv_size - 1] = '\0'; // Nul termination
					strncpy(msg, buf, rcv_size);
					msg[rcv_size - 1] = '\0'; // Nul termination
				}
				
				/* Command parsing */
				if (iscmd(msg, strlen(msg))) {
					char *pbuf = strtok(buf, " ");
					
					// Close command
					if (strncmp(pbuf + 1, msg_cmd[MSG_CLOSE], strlen(msg_cmd[MSG_CLOSE])) == 0) {
						sendBye(sock_fd);
						close(sock_fd);
						goto SUCCESS;
					// File command
					} else if (strncmp(pbuf + 1, msg_cmd[MSG_FILE], strlen(msg_cmd[MSG_FILE])) == 0) {
						pbuf = strtok(NULL, " ");
						
						// /file ls <username>
						if (strncmp(pbuf, "ls", 2) == 0) {
							char username[BUFF_SIZE];
							
							pbuf = strtok(NULL, " ");
							snprintf(username, BUFF_SIZE, "%s", pbuf);
							
							if (sendFileLsReq(sock_fd, username, strlen(username)) == -1) {
								perror("Failed sendFileLsReq()");
							}
						// /file dn <username> <filename>
						} else if (strncmp(pbuf, "dn", 2) == 0) {
							char username[BUFF_SIZE];
							char filename[BUFF_SIZE];
							
							pbuf = strtok(NULL, " ");
							snprintf(username, BUFF_SIZE, "%s", pbuf);
							
							pbuf = strtok(NULL, " ");
							snprintf(filename, BUFF_SIZE, "%s", pbuf);
							
							if (sendFileDnReq(sock_fd, username, strlen(username), filename, strlen(filename)) == -1) {
								perror("Failed sendFileDnReq()");
							}
						} else {
							printf("Unknown file command %s\n", msg + 1);
						}
					} else {
						printf("Unknown command %s\n", msg + 1);
					}
				} else {
					if (sendMsg(sock_fd, msg, strlen(msg) + 1) == -1) {
						perror("Failed send()");
						exit(EXIT_FAILURE);
					}
				}
			/* Socket event */
			} else if (events[i].data.fd == sock_fd) {
                rcv_size = recv(sock_fd, buf, sizeof(buf), 0);
                if (rcv_size == -1) {
                    perror("Failed read() from socket");
                    exit(EXIT_FAILURE);
				} else if (rcv_size == 0) {
					printf("Gentle close");
					close(events[i].data.fd);
					exit(EXIT_SUCCESS);
                } else {
                    buf[rcv_size] = '\0'; // Nul termination
					strncpy(msg, buf, rcv_size);
					msg[rcv_size - 1] = '\0'; // Nul termination
                }
				
				// MSG_CHAT
				if (msg[0] == MSG_CHAT + '0') {
					printf("%s\n", msg + 1);
				// MSG_FILE
				} else if (msg[0] == MSG_FILE + '0') {
					char *pbuf = strtok(buf, " ");
					
					// MSG_FILE_LS_REQ
					if (msg[1] == MSG_FILE_LS_REQ + '0') {
						testPath_storage();
						
						if (sendFileLs(sock_fd, pbuf + 2, strlen(pbuf + 2)) == -1) {
							perror("Failed sendFileLs()");
						}
					// MSG_FILE_LS_START
					} else if (msg[1] == MSG_FILE_LS_START + '0') {
						printf("===== FILE LIST START =====\n");
					// MSG_FILE_LS_BODY
					} else if (msg[1] == MSG_FILE_LS_BODY + '0') {
						pbuf = strtok(NULL, " ");
						printf(" %s\n", pbuf);
					// MSG_FILE_LS_END
					} else if (msg[1] == MSG_FILE_LS_END + '0') {
						printf("===== FILE LIST END =======\n");
					// MSG_FILE_DN_REQ
					} else if (msg[1] == MSG_FILE_DN_REQ + '0') {
						const char *ipaddr_dummy = "127.0.0.1";
						bool check_file = false;
						bool check_socket = false;
						bool send_ack = false;
						int p2p_sockfd;
						int thr_id;
						char username[BUFF_SIZE];
						char filename[BUFF_SIZE];
						char port_str[BUFF_SIZE];
						
						snprintf(username, BUFF_SIZE, "%s", pbuf + 2);
						
						pbuf = strtok(NULL, " ");
						snprintf(filename, BUFF_SIZE, "%s", pbuf);
						
						printf("filename: %s\n", filename);
						
						check_file = isFileExist_storage(filename, strlen(filename));
						printf("file: %d\n", check_file);
						
						p2p_sockfd = init_server(P2P_PORT, BACKLOG);
						if (p2p_sockfd == -1) {
							perror("Failed init_server()");
						} else {
							check_socket = true;
						}
						
						send_ack = (check_file && check_socket) ? true : false;
						if (send_ack) {
							// ACK
							pthread_t p2p_thread;
							
							snprintf(port_str, BUFF_SIZE, "%d", P2P_PORT);
							
							thr_id = pthread_create(&p2p_thread, NULL, p2p_thread_sender, (void*)&p2p_sockfd);
							if (thr_id == -1) {
								perror("Failed pthread_create()");
								break;
							} else {
								printf("P2P sender thread is OK...\n");
								
								pthread_detach(p2p_thread);
							
								if (sendFileDnAck(sock_fd, username, strlen(username),\
									 filename, strlen(filename), ipaddr_dummy, strlen(ipaddr_dummy), port_str, strlen(port_str)) == -1) {
									perror("Failed sendFileDnAck()");
								}
							}
						} else {
							// NACK
							printf("Close P2P socket\n");
							close(p2p_sockfd);
							
							if (sendFileDnNack(sock_fd, username, strlen(username)) == -1) {
								perror("Failed sendFilednNack()");
							}
						}
					// MSG_FILE_DN_ACK
					} else if (msg[1] == MSG_FILE_DN_ACK + '0') {
						int sock_p2p;
						char username[BUFF_SIZE];
						char filename[BUFF_SIZE];
						char ipaddr[BUFF_SIZE];
						char port_str[BUFF_SIZE];
						int port_num;
						struct tparam param;
						pthread_t file_thread;
						
						snprintf(username, BUFF_SIZE, "%s", pbuf + 2);
						
						pbuf = strtok(NULL, " ");
						snprintf(filename, BUFF_SIZE, "%s", pbuf);
						
						pbuf = strtok(NULL, " ");
						snprintf(ipaddr, BUFF_SIZE, "%s", pbuf);
						
						pbuf = strtok(NULL, " ");
						snprintf(port_str, BUFF_SIZE, "%s", pbuf);
						
						port_num = atoi(port_str);
						
						sock_p2p = connect_server(ipaddr, port_num);
						if (sock_p2p == -1) {
							perror("Failed connect_server()");
						} else {
							param.sock_fd = sock_p2p;
							strncpy(param.filename, filename, strlen(filename));
							param.filename[strlen(filename)] = '\0';
							
							pthread_create(&file_thread, NULL, p2p_thread_receiver, (void*)&param);
							pthread_detach(file_thread);
						}
						
						printf("ACK: %s\n", msg);
					// MSG_FILE_DN_NACK
					} else if (msg[1] == MSG_FILE_DN_NACK + '0') {
						printf("file not found\n");
					// Unknown _msg_type_file
					} else {
						printf("Unknown file command %s\n", msg);
					}
				// MSG_CLOSE
				} else if (msg[0] == MSG_CLOSE + '0') {
					printf("Server close socket\n");
					close(events[i].data.fd);
					goto SUCCESS;
				// Gentle exit
				} else if (msg[0] == 0) {
					printf("Server close\n");
					close(events[i].data.fd);
					exit(EXIT_SUCCESS);
				// Unknown msg
				} else {
					printf("Unknown msg\n");
					printf("recv: %zd %s\n", rcv_size, msg);
					close(events[i].data.fd);
					exit(EXIT_FAILURE);
				}
            }
		}
	}

SUCCESS:	
    close(sock_fd);

	return 0;
}

int connect_server(const char *ip, unsigned short port) {
    int sock_fd;
    struct sockaddr_in dest_addr;
    socklen_t addr_len = sizeof(struct sockaddr);

    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd == -1) {
        perror("Failed socket()");
        
		goto fail;
    } else {
        printf("Client socket() is OK...\n");
    }

    // Host byte order
    dest_addr.sin_family = AF_INET;

    // Short, network byte order
    dest_addr.sin_port = htons(port);
    dest_addr.sin_addr.s_addr = inet_addr(ip);

    // Padding zeto to the rest of the struct
    memset(&(dest_addr.sin_zero), 0, sizeof(dest_addr.sin_zero));

    // Connect
    if (connect(sock_fd, (struct sockaddr*)&dest_addr, addr_len) == -1) {
        perror("Failed connect()");
        
		goto fail;
    } else {
        printf("Client connect() is OK...\n");
    }

success:
    return sock_fd;
fail:
	return -1;
}

int init_server(unsigned short port, int backlog) {
	// Listen on sock_fd
	int sock_fd;

	// My address information
	struct sockaddr_in my_addr;
	
	// Create new socket
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_fd == -1) {
		perror("Failed socket()");
		goto fail;
	} else {
		printf("P2P Server socket() sock_fd is OK...\n");
	}

	// Host byte order
	my_addr.sin_family = AF_INET;

	// Short, network byte order
	my_addr.sin_port = htons(port);
	my_addr.sin_addr.s_addr = INADDR_ANY;

	// Zero the rest of the struct
	memset(&(my_addr.sin_zero), 0, sizeof(my_addr.sin_zero));

	// Bind
	if (bind(sock_fd, (struct sockaddr*)&my_addr, sizeof(struct sockaddr)) == -1) {
		perror("Failed bind()");
		close(sock_fd);
		
		goto fail;
	} else {
		printf("P2P Server bind() is OK...\n");
	}

	// Listen
	if (listen(sock_fd, backlog) == -1) {
		perror("Failed listen()");
		close(sock_fd);
		
		goto fail;
	} else {
		printf("P2P Server listen() is OK...\n");
	}

success:
	return sock_fd;
fail:
	return -1;
}
