#pragma once

#include <stdbool.h>

#define CRD_BUFF_SIZE 50

typedef struct _credential_t {
    char username[CRD_BUFF_SIZE];
    char password[CRD_BUFF_SIZE];
} credential_t;

extern bool auth(const char*, size_t, const char*, size_t);

static credential_t* getUserInfo(FILE*, const char*, size_t);